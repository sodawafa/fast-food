import React, { Component } from 'react'
import arr from '../assets/Vector.png'
import { Link } from 'react-router-dom'
import '../css/recette/collapse.css'

class Collapse extends Component {
  active = true

  constructor (props) {
    super(props)
    this.state = {
      name: props.name,
      text: props.text,
      array: props.array,
      active: this.active,

    }
    this.handleClick = this.handleClick.bind(this)

  }

  handleClick (event) {
    event.preventDefault()
    this.setState({
      active: !this.state.active,

    })

  }

  contentText () {
    if (this.state.text !== undefined) {
      return (
        <div>
          <p>{this.state.text}</p>
        </div>

      )
    }
    return null
  }

  contentArray () {
    if (this.state.array !== undefined) {
      return (
        <div>
          <ul>
            {
              this.state.array &&
              this.state.array.map((elem, index) => {
                return (
                  <li key={'collapse_content' + index}>{elem}</li>
                )
              })
            }

          </ul>
        </div>

      )
    }
    return null
  }

  render () {
    return (
      <div className={`collapse ${this.state.active ? 'active' : ''}`}
           key={this.props._key}
      >
        <header>
          <Link to={'/#'} onClick={this.handleClick}>
            {this.state.name} <img src={arr} alt=""/>
          </Link>


        </header>
        {this.state.active ? this.contentText() : null}
        {this.state.active ? this.contentArray() : null}


      </div>
    )
  }
}

export default Collapse
