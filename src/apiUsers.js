export const BaseUrl = 'http://localhost:3002/api/v1'
let apiUsers = async function(url,username,password){
  if (!url){
    return;
  }
  return await fetch(
    BaseUrl+ url,
    {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'username': username,
        'password': password
      })
    },
  ).then(results => {
    return results.json()
/*
    const contentType = results.headers.get('content-type')
    if(results.ok && contentType && contentType.includes('application/json')){
      return results.json()
    } else {

    }
*/
  })
}
export default apiUsers
