export const HEADERS = (token = '') => {
  return new Headers({
    'content-Type': 'application/json',
    'Authorization': `Bearer${token}`,

  })
}
/*const PORT = 3002
const PROTOCOL = 'http'
const DOMAIN_NAME = 'localhost'
const API_VERSION = '/api/v1'
export const API_URL = (PROTOCOL ? (PROTOCOL + '://') : '') + DOMAIN_NAME +
  (PORT ? (':' + PORT) : '') + API_VERSION*/
