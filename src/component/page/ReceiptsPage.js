import React, { Component } from 'react'
import api from '../../api'
import '../../css/recette/receipt.css'
import { Link } from 'react-router-dom'
import '../../css/header/search.css'
import search from '../../assets/searchIcon.png'

class ReceiptsPage extends Component {

  constructor (props) {
    super(props)
    this.state = {
      receipts: [],
      search: '',

    }
  }

  componentWillMount () {
    api('/receipts').then(json => {
      this.setState({ receipts: json.data })
    })

  }

  updateSearch (event) {
    this.setState({ search: event.target.value })
  }

  render () {
    let filteredContacts = this.state.receipts.filter(
      (receipt) => {
        return receipt.name.toLowerCase()
          .indexOf(this.state.search.toLowerCase()) !== -1

      },
    )

    return (
      <div className={'receipts-content'}>
        <header className={'section-header'}>
          <div>
            <input type={'text'} value={this.state.search}
                   onChange={this.updateSearch.bind(this)}
                   placeholder={'Rechercher une recette'}/>

          </div>
          <div className={'search'}>

            <Link to={'#'}><img src={search}/></Link>
          </div>

        </header>
        <div className={'receipts'}>
          {
            filteredContacts.length > 0 &&
            filteredContacts.map((receipt, index) => {
              return (

                <div key={'receipts_' + receipt.id + index.id} className={'receipts-items'}>
                  <Link to={`/receipt/${receipt.id}`}>
                    <img
                      src={require(`./../../assets${receipt.picture}`).default}
                      alt={receipt.name} />
                  </Link>

                      <h4 className={'title'}>{receipt.name}</h4>


                </div>
              )
            })
          }

        </div>

      </div>
    )

  }
}

export default ReceiptsPage

