import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Logo from '../header/Logo'
import sign from '../../assets/userSeign.png'
import signIn from '../../assets/userSeign.png'
import '../../css/login/login.css'
import apiUsers, { BaseUrll } from '../../apiUsers'

class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      submitted: false,

    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

  }

  handleChange (e) {
    const { name, value } = e.target
    this.setState({ [name]: value })

  }

  handleSubmit (event) {
    event.preventDefault()
    apiUsers('/login', this.state.username, this.state.password).then(json => {
      if (json.token){
        window.location.href = "/?#/products"
      }

    })

    };



  render () {
    return (
      <div>
        <header>
          <nav className={'main-nav'}>
            <Link to={'/#'}><Logo/></Link>
            <div>
              <Link className="main-nav-item" to="">
                <img src={sign} className={'icon'} alt="signIn"/>
                Sign In
              </Link>
            </div>

          </nav>
        </header>
        <main className={'main-bg'}>
          <section className={'sign-in-content'}>
            <img src={signIn} className={'icon-sign'} alt="sign"/>
            <h1>Sign In</h1>
            <form onSubmit={this.handleSubmit}>
              <div className={'input-wrapper'}>
                <label htmlFor={'username'}>Username</label>
                <input type={'text'} id={'username'} name={'username'}
                       onChange={this.handleChange} />

              </div>
              <div className={'input-wrapper'}>
                <label htmlFor={'password'}>Password</label>
                <input type={'password'} id={'password'} name={'password'}
                       onChange={this.handleChange}/>

              </div>
              <button type={'submit'} className={'sign-in-btn'}>Sign In</button>
            </form>


          </section>
        </main>
      </div>
    )
  }
}


export default Login
