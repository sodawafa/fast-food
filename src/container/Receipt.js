import React, { Component } from 'react'
import api from '../api'
import '../css/recette/receipt.css'
import Collapse from './Collapse'
import { Link } from 'react-router-dom'
import arrow from '../assets/left-arrow.png'
class Receipt extends Component{
  active = true
  constructor (props) {
    super(props)
    this.state={
      receipt: [],


    }
  }
  componentWillMount () {
    api('/receipt/' + this.props.id).then(json => {
      this.setState({receipt: json.data})
    })
  }



  render () {
    console.log(this.state.receipt.name)
    return(
      <div className={'receipts-content' }>
        <header>
        <h1>
        {this.state.receipt.name}
        {/*  <div className={'content-arrow'}>
            <Link to={'/product'}><img className={'arrow'} src={arrow} alt={this.state.receipt.name}/></Link>
          </div>*/}


        </h1>
        </header>
        <div className={'receipts-collapse'}>
        <ul>
          <Collapse name={'ingredient'} array={this.state.receipt.ingredient}
                    key={'collapses_ingredient_' + this.state.receipt.id}
                    _key={'collapse_ingredient_' + this.state.receipt.id}


          />


       {/*   <h2>Ingredient : </h2>
          <ul>
            {
              this.state.receipt.ingredient && this.state.receipt.ingredient.map(
                (elem, index) => {
                  return (
                    <li key={'receipts_content_' + index}>{elem}</li>
                  )
                },
              )
            }
          </ul>*/}
        </ul>

        <ul>
          <Collapse name={'preparation'}
                    array={this.state.receipt.preparation}
                    key={'collapses_preparation_' + this.state.receipt.id}
                    _key={'collapse_preparation_' + this.state.receipt.id}


          />
    {/*    <h2>Préparation : </h2>
          <ul>
            {
              this.state.receipt.preparation && this.state.receipt.preparation.map(
                (elem,index) => {
                  return(
                    <li key={'receipts_content_' + index}>{elem}</li>
                  )
                }
              )
            }
          </ul>*/}

        </ul>
        </div>

      </div>
    )
  }
}
export default Receipt
