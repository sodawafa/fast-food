import { HEADERS} from '../../redux/constants/APIConfig'
import {BaseUrl} from '../../apiUsers'
import apiUsers from '../../apiUsers'

export const userService = {
  profile,
  login,
  logout,
  signup,
  updateProfile,
  isAuthenticated
}

function login (username, password) {
  const requestOptions = {
    method: 'POST',
    header: HEADERS(),
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  }
  return apiUsers(`login`,requestOptions).then(handleResponse)
    .then(response => {
      console.log(response)
      if (response.status === 200) {
        const user = {
          username: username,
          token: response.body.token ? response.body.token : '',
        }
        return this.profile(user).then(userInfo => {
            const myUser = {
              ...user,
              ...userInfo,
            }
            localStorage.setItem('user', JSON.stringify(myUser))
          },
          error => Promise.reject(error))
      }
    })

}

function logout () {
  localStorage.removeItem('user')

}

function signup (firstName, lastName, username, password) {
  const requestOptions = {
    method: 'POST',
    header: HEADERS(),
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      username: username,
      password: password,
    }),
  }
  return fetch(`${BaseUrl}/signUp`, requestOptions)
    .then(handleResponse)
    .then(response => {
      if (response.status === 200) {
        const username = response.body.username
        return username

      }

    })

}

function updateProfile (user, firstName, lastName) {
  const requestOptions = {
    method: 'PUT',
    header: HEADERS(user.token),
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
    }),
  }
  return fetch(`${BaseUrl}/profile`, requestOptions)
    .then(handleResponse)
    .then(response => {
        if (response.status === 200) {
          const userInfo = response.body
          const myUser = {
            ...user,
            ...userInfo,
          }
          localStorage.setItem('user', JSON.stringify(myUser))
          return myUser
        }

      },
      error => Promise.reject(error))

}

function profile (user = null) {
  const requestOptions = {
    method: 'POST',
    header: HEADERS(user.token),
  }
  return fetch(`${BaseUrl}/profile`, requestOptions)
    .then(handleResponse)
    .then(response => {
      const userInfo = response.body
      localStorage.setItem('user', JSON.stringify({
        ...user,
        ...userInfo,
      }))
      return response.body
    })

}
function isAuthenticated () {
  const json = localStorage.getItem('user')
  const user = json ? JSON.parse(json) : null
  return (user && user.token) ? user : null

}

function handleResponse (response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text)
    if (!response.ok) {
      if (response.status === 401) {
        logout()
        window.location.reload(true)
      }
      const error = (data && data.message) || response.statusText
      return Promise.reject(error)
    }
  })

}

