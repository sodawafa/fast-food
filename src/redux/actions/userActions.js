import { userService } from '../service/user.service'
import { ACTION_TYPES } from '../constants/userActionTypes'
import store from '../store'

export const userAction = {
  login,
  signup,
  profile,
  logout,
  updateProfile,
  isAuthenticated
}

function login (props, username, password) {
  return dispatch => {
    dispatch(request({ username }))
    userService.logout()
    userService.login(username, password).then(user => {
        dispatch(success(user))
        props.history.push('/user')
      },
      error => dispatch(failure(error)))
  }

  function request (user) {
    return {
      type: ACTION_TYPES.LOGIN_REQUEST,
      user,
    }

  }

  function success (user) {
    return {
      type: ACTION_TYPES.LOGIN_SUCCESS,
      user,
    }

  }

  function failure (error) {
    return {
      type: ACTION_TYPES.LOGIN_FAILURE,
      error,
    }

  }

}
function logout (props) {
userService.logout()
props.history.push('/')
  return {
  type: ACTION_TYPES.LOGOUT
  }

}

function signup (props, firstName, lastName, username, password) {
  return dispatch => {
    dispatch(request(username))
    userService.logout()
    userService.signup(firstName, lastName, username, password)
      .then(username => {
          dispatch(success(username))
          props.history.push('/login')

        },
        error => dispatch(failure(error)),
      )
  }

  function request (user) {
    return {
      type: ACTION_TYPES.REGISTER_REQUEST,
      user,
    }

  }

  function success (user) {
    return {
      type: ACTION_TYPES.REGISTER_SUCCESS,
      user,
    }

  }

  function failure (error) {
    return {
      type: ACTION_TYPES.REGISTER_FAILURE,
      error,
    }

  }

}

function profile (user) {
  return dispatch => {
    dispatch(request(user))
    userService.profile(user).then(user =>
      dispatch(success(user)),
      error => dispatch(failure(error))
    )
  }

  function request (user) {
    return {
      type: ACTION_TYPES.PROFILE_REQUEST,
      user,
    }

  }

  function success (user) {
    return {
      type: ACTION_TYPES.PROFILE_SUCCESS,
      user,
    }

  }

  function failure (error) {
    return {
      type: ACTION_TYPES.PROFILE_FAILURE,
      error,
    }

  }
}
function updateProfile (props,user,firstName,lastName) {
  return dispatch => {
    dispatch(request(user))
    userService.updateProfile(firstName,lastName).then(user => {
      dispatch(success(user))

    },
      error => dispatch(failure(error)))
  }
  function request (user) {
    return {
      type: ACTION_TYPES.PROFILE_REQUEST,
      user
    }

  }
  function success (user) {
    return {
      type: ACTION_TYPES.PROFILE_SUCCESS,
      user
    }

  }
  function failure (error) {
    return {
      type: ACTION_TYPES.PROFILE_FAILURE,
      error
    }

  }

}
function isAuthenticated () {
  return dispatch => {
    let user = store.getState().userReducer.user
    user = user ? user : userService.userReducer()
    user && dispatch(success(user))
    return user
  }
  function success(user){
    return {
      type: ACTION_TYPES.USER_AUTHENTICATED,
      user
    }
  }
}


