import React, { Component } from 'react'
import '../../css/page/produitspage.css'
import api from '../../api'
import { Link } from 'react-router-dom'
import Logo from '../header/Logo'
import Nav from '../header/Nav'
class ProductsPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      products: [],
    }
  }

  componentWillMount () {
    api('/products').then(json => {
      this.setState({ products: json.data })

    })

  }

  render () {

    return (

      <div className={'products-content'}>
        <nav className={'products-content-nav'}>
        <Link to={'/#'}><Logo/></Link>
       <Nav/>
        </nav>
        <div className={'products'}>
          {
            this.state.products.length > 0 &&
            this.state.products.map((product,index) => {
              return (
                <ul key={'product_' + product.id + index.id}>
                  <li><Link to={`/product/${product.id}`}><img
                    src={require(`./../../assets${product.picture}`).default}
                    alt={product.name} width={250}/></Link></li>
                  <li className={'products-item'}>{product.name}</li>
                </ul>
              )
            })
          }
        </div>
      </div>
    )
  }
}

export default ProductsPage
