import React, { Component } from 'react'
import '../css/container/product.css'
import api from '../api'
import Carousel from './Carousel'
import { Link } from 'react-router-dom'
import arrow from '../assets/left-arrow.png'
import Collapse from './Collapse'

class Product extends Component {
  constructor (props) {
    super(props)
    this.state = {
      product: [],

    }
  }

  componentWillMount () {
    api('/product/' + this.props.id).then(json => {
      this.setState({ product: json.data })

    })
  }

  render () {
    return (
      <section className={'content'}>

        <header>
          <h1>{this.state.product.name}</h1>
          <div className={'content-arrow'}>
            <Link to={'/products'}><img className={'arrow'} src={arrow} alt={this.state.product.name}/></Link>
          </div>

        </header>
        <nav>
          <div>

          <ul>
            <Collapse name={'ingredient'} array={this.state.product.ingredient}  key={'collapses_ingredient_' + this.state.product}
                      _key={'collapse_ingredient_' + this.state.product}/>

     {/*       <h2>Ingredient:</h2>
            <ul className={"ingredient-list"}>
              {
                this.state.product.ingredient &&
                this.state.product.ingredient.map((elem, index) => {
                  return (
                    <li className={'ingredient-list-items'} key={'product_content_' + index}>{elem}</li>
                  )
                })
              }
            </ul>*/}
          </ul>
            <ul>
              <Collapse name={'price'} text={this.state.product.price + this.state.product.unit} key={'collapses_ingredient_' + this.state.product}
                        _key={'collapse_ingredient_' + this.state.product}/>
            </ul>
            <ul>
              <Collapse name={'Valeur Enérgetique'} array={this.state.product.energetic}key={'collapses_ingredient_' + this.state.product}
                        _key={'collapse_ingredient_' + this.state.product}/>
            </ul>
          </div>


        </nav>
        <main>
          {/*{ React.createElement('h1', null, `Bonjour ${this.state.product.id}`) }*/}
          <Carousel key={'carousel_100'} pictures={this.state.product.pictures}/>
                   {/* <div className={'pictures-content'}>
           {
              this.state.product.pictures &&
              this.state.product.pictures.map((picture, index) => {
                return (
                  <div className={'pictures'} key={'pictures_x_' + index} >
                    <img src={require(`./../assets${picture}`).default} alt=""/>
                  </div>
                )
              })

            }
          </div>*/}
          <button type={"submit"} className={'btn'}>Ajouter au panier</button>

        </main>

      </section>
    )
  }
}

export default Product
