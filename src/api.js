export const BaseURL = 'http://localhost:3002/api/v1'
let api = async function (url) {
  if(!url){
    return;
  }
  return await fetch(
    BaseURL + url,
    {
      method: 'GET',
      headers: {},
    },
  ).then(results => {
    const contentType = results.headers.get('content-type')
    if (results.ok && contentType && contentType.includes('application/json')) {
      return results.json()
    } else {
      throw new TypeError('Problem with JSON: ' + results.error)
    }
  })
}

export default api


