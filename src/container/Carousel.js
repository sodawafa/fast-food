import React, { Component } from 'react'
import '../css/container/carousel.css'

class Carousel extends Component {
  constructor (props) {
    super(props)
    this.state = {
      counter: 0,
    }
    this.next = this.next.bind(this)
    this.prev = this.prev.bind(this)

  }

  shouldComponentUpdate (nextProps, nextState, nextContext) {
    /*console.log('[Counter] - [shouldComponentUpdate]')
    console.log(
      '[Counter] - [shouldComponentUpdate] - [nextProps]',
      nextProps,
    )
    console.log(
      '[Counter] - [shouldComponentUpdate] - [nextState]',
      nextState,
    )
    console.log(
      '[Counter] - [shouldComponentUpdate] - [nextContext]',
      nextContext,
    )*/
    if (nextProps.pictures && nextProps.pictures.length && !this.state.pictures) {
      this.setState({
        pictures: nextProps.pictures,
        picture: require(`./../assets${nextProps.pictures[0]}`).default,
      })
    }
    return true
  }

  next (e) {
    let counter = this.state.counter + 1
    if (counter >= this.state.pictures.length) {
      counter = 0
    }
    this.setState({
      counter: counter,
      picture: require(`./../assets${this.state.pictures[counter]}`).default,
    })
    e.preventDefault()
  }

  prev (e) {
    let counter = this.state.counter - 1
    if (counter < 0) {
      counter = this.state.pictures.length - 1
    }
    this.setState({
      counter: counter,
      picture: require(`./../assets${this.state.pictures[counter]}`).default,
    })
    e.preventDefault()
  }

  nextBtn () {
    return <button className={'carousel_next'} onClick={this.next}></button>
  }

  prevBtn () {
    return <button className={'carousel_prev'} onClick={this.prev}></button>
  }

  render () {
    if (this.state.picture) {
      return (
        <div className={'carousel'}
             style={{ backgroundImage: `url(${this.state.picture})` }}>
          {
            this.nextBtn()
          }
          {
            this.prevBtn()
          }
          <div className={'carousel_number'}>
            {this.state.counter + 1}/{this.state.pictures.length}
          </div>

        </div>
      )
    } else {
      return <div>

      </div>
    }
  }
}

export default Carousel
