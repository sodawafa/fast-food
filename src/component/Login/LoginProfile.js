import React, { Component } from 'react'
import '../../css/login/loginprofile.css'
import { Link } from 'react-router-dom'
import {connect} from 'react-redux'
import { userAction } from '../../redux/actions/userActions'
import apiUsers from '../../apiUsers'

class LoginProfile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      submitted:false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

  }
  handleChange(e) {
    const {name,value} = e.target
    this.setState({[name]: value})

  }
/*
  handleSubmit (event) {
    event.preventDefault()
    apiUsers('/login', this.state.username, this.state.password).then(json => {
      if (json.token){
        window.location.href = "/?#/products"
      }

    })

  };*/
  handleSubmit(event){
    event.preventDefault()
    this.setState({submitted:true})
    const {username,password} = this.state
    const {dispatch} = this.props
       if (username && password){
      dispatch(userAction.login(this.props,username,password))
    }


  }




  render () {
    const {loggedIn} = true
    const {username,password,submitted} = this.state
    return (
      <div>
        <section className={'page'}>
          <header>
            <div className={'page-title'}>
              <h1>BIENVENUS !</h1>
            </div>


          </header>
          <nav>
            <div className={'raison-inscription'}>
              <div className={'bloc-even'}>
                <h2>7 bonnes raisons de vous abonner :</h2>
                <ol>
                  <li>
                    <strong>L’accès libre à la plus importante base de données
                      d’articles sur la nutrition.</strong>
                    <br/>
                    "Consultez plus de 4500 articles sur l’alimentation et la
                    santé à tout moment pendant la durée de votre abonnement."
                  </li>
                  <li>
                    <strong>
                      La recherche décryptée pour vous
                    </strong>
                    <br/>
                    Vous n’avez pas le temps de lire les journaux scientifiques,
                    de fréquenter les congrès ? LaNutrition.fr le fait pour vous
                    avec objectivité et dans un langage clair, références à
                    l’appui.
                  </li>
                  <li>
                    <strong>
                      Des entretiens en profondeur avec les plus grands
                      chercheurs
                    </strong>
                    <br/>
                    Ceux qui font avancer la recherche partagent avec nos
                    internautes leurs découvertes.
                  </li>
                  <li>
                    <strong>
                      Des tests comparatifs exclusifs
                    </strong>
                    <br/>
                    Quels aliments acheter ou conseiller ? Quels compléments
                    alimentaires ? LaNutrition.fr analyse, teste et compare pour
                    vous.
                  </li>
                  <li>
                    <strong>
                      Les réponses à vos questions
                    </strong>
                    <br/>

                    Nos experts répondent dans les meilleurs délais à vos
                    questions.
                  </li>
                  <li>
                    <strong>
                      Votre analyse ou votre point de vue publiés
                    </strong>
                    <br/>
                    Envie d’exprimer une analyse ou un point de vue ? Nous
                    publions votre contribution sur le site dès lors qu’elle
                    respecte la charte éditoriale.
                  </li>
                  <li>
                    <strong>
                      Les bonnes feuilles des livres à paraître
                    </strong>
                    <br/>
                    Vous recevez en avant-première les extraits de livres
                    importants dans le domaine de la nutrition et la santé.
                  </li>

                </ol>

              </div>

            </div>


          </nav>
          <main>
            <form action="user" className={'user-login'} onSubmit={this.handleSubmit}>
              <div>
                <div className={'wrapper-right'}>
                  <div className={'creat-account'}>
                    <h2>Pas encore inscrit ?</h2>
                    <Link to="/registration" className={'login-register'}>CRÊE UN
                      COMPTE</Link>
                  </div>
                  <div className={'user-login-form'}>
                    <h2>Vous avez déjà un compte ?</h2>
                    <div className={'form-item' + (submitted && !username ? 'has-error' : '') } >
                      <input type="text" placeholder={'Email'} name={'email'} onChange={this.handleChange}/>
                      {submitted && !username &&
                      <div className={'required'}>Username is required</div>
                      }

                    </div>
                    <div className={'form-item' + (submitted && !password ? 'has-error' : '')}>
                      <input type="password" placeholder={'Mot de passe'}
                             name={'password'} onChange={this.handleChange}/>
                      {submitted && !password &&
                      <div className={'required'}>Password is required</div>
                      }

                    </div>
                    <Link to={'#'} className={'form-item'}>Mot de passe
                      oublié</Link>
                    <div className={'form-btn'}>
                      <button className={'edit-submit'} type={'submit'}>SE
                        CONNECTER
                      </button>

                    </div>

                  </div>
                  <div className={'avis-abonnes'}>
                    <div className={'bloc-first'}></div>
                    <h2>
                      <span>Edition abonné : ce que vous allez apprendre</span>
                    </h2>
                    <p>
                      <font size={'4'}>
                        <strong>Le lait de soja est-il dangereux pour les
                          enfants ?</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong> Le traitement naturel de l’ostéoporose</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Les méthodes antistress naturelles qui ont fait leur preuve</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Les comportements qui favorisent le risque de cancer</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong> Pourquoi une alimentation riche en graisse est conseillée pour combattre le cancer</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Comment choisir un complément alimentaire ?</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Comment combattre l’arthrose naturellement</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Les dernières méthodes efficaces et les recherches</strong>
                      </font>
                    </p>

                    <p>
                      <font size={'4'}>
                        <u>
                        <strong>Prenez soin de votre santé et de celle de vos proches grâce à une information claire et objective, basée sur les dernières études scientifiques.</strong>
                        </u>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong> Les aliments qui combattent l’inflammation</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Comment choisir un complément alimentaire ?</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Les meilleures marques de lait infantiles</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong> Pourquoi une alimentation cétogène est conseillée pour combattre le cancer</strong>
                      </font>
                    </p>
                    <p>
                      <font size={'4'}>
                        <strong>Les édulcorants de synthèse à éviter absolument</strong>
                      </font>
                    </p>
                    <h2><span>Les abonnés jugent LaNutrition.fr</span></h2>
                    <ul>
                      <li><strong>Suzanne B.</strong><br/>Félicitations pour votre site et merci pour toutes les précieuses informations que vous partagez avec le public.</li>
                      <li><strong>Denis D.</strong><br/>Merci pour votre travail et vos informations, bravo et continuez vos efforts, je peux vous dire qu'ils ne sont pas vains.</li>
                      <li><strong>Patricia V.</strong><br/>Comme ancien professionnel de la santé en retraite je tenais à vous dire bravo et toutes mes félicitations.</li>
                      <li><strong>Jean-Pierre T.</strong><br/>Merci d'avoir de votre côté toujours le souci de l'éthique, du bon sens et de la vérité !</li>
                      <li><strong>Caroline C.</strong><br/>Bravo pour votre site qui est une mine d'information.</li>

                    </ul>


                  </div>

                </div>

              </div>
            </form>


          </main>
          <footer>

          </footer>

        </section>

      </div>
    )
  }
}

const mapStateToProps = state => ({
  loading: state.userReducer.loading,
})

const connectedLoginPage = connect(mapStateToProps)(LoginProfile)
export { connectedLoginPage as LoginProfile }
