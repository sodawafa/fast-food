import '../../css/header/logo.css'
import logo from '../../assets/img_24.png'
import React from 'react'
import { Link } from 'react-router-dom'

export default function Logo () {
  return (
    <div className={'header'}>
      <img className={'header-logo'} src={logo} alt="fast-food"/>
    </div>

  )
}

