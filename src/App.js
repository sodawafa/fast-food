import React, { Component } from 'react'
import Logo from './component/header/Logo'
import Nav from './component/header/Nav'
import ProductsPage from './component/page/ProductsPage'
import ReceiptsPage from './component/page/ReceiptsPage'
import RecrutementPage from './component/page/RecrutementPage'
import EntreprisePage from './component/page/EntreprisePage'
import NutritionPage from './component/page/NutritionPage'
import Recettes from './container/Receipt'
import {
  Switch,
  Route,
  HashRouter,
} from 'react-router-dom'

import HomePage from './component/page/HomePage'
import Product from './container/Product'
import Receipt from './container/Receipt'
import Login from './component/Login/Login'
import {LoginProfile} from './component/Login/LoginProfile'
import Energie from './container/Energie'
import Registration from './component/Login/Registration'

class App extends Component {
  constructor (props) {
    super(props)

  }


  render () {
    return (


        <HashRouter>
          <Switch>
            <Route path="/products">
              <ProductsPage/>
            </Route>
            <Route path={'/product/:id'}
                   render={(props) => (
                     <Product id={props.match.params.id}/>
                   )}/>
            <Route path="/receipts">
              <ReceiptsPage/>
            </Route>
            <Route path={'/receipt/:id'} render={(props) => (
              <Receipt id={props.match.params.id}/>
            )}/>
            <Route path="/energies">
              <NutritionPage/>
            </Route>
            <Route path={'/energie/:id'}
                   render={(props) => (
                     <Energie id={props.match.params.id}/>
                   )}/>

            <Route path="/entreprise">
              <EntreprisePage/>
            </Route>
            <Route path="/recrutement">
              <RecrutementPage/>
            </Route>
            <Route  path="/login" component={Login}/>
            <Route path="/loginprofile" component={LoginProfile}/>
            <Route path="/registration" component={Registration}/>
            <Route path={"/"}>
              <header>
                {/*<Logo/>*/}
                {/*<Nav/>*/}
              </header>
              <HomePage/>
            </Route>





          </Switch>

        </HashRouter>


    )
  }
}

export default App
