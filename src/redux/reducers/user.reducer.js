import {ACTION_TYPES} from '../constants/userActionTypes'
let userCookie = JSON.parse(localStorage.getItem('user'))
let initialState = userCookie ? {
  loggedIn: true,
  user: userCookie,
  username:null
}: {}
initialState = {...initialState}
export function userReducer (state = initialState,action) {
  switch (action.type) {
    case ACTION_TYPES.PROFILE_REQUEST :
      return {
        loggedIn: true
      }
    case ACTION_TYPES.PROFILE_SUCCESS :
      return {
        user: action.user

      }
    case ACTION_TYPES.PROFILE_FAILURE :
      return {
        error: action.error
      };
    case ACTION_TYPES.LOGIN_REQUEST:
      return {
        loggedIn: true,
        user: action.user
      };
    case ACTION_TYPES.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case ACTION_TYPES.LOGIN_FAILURE:
      return {};
    case ACTION_TYPES.LOGOUT:
      return {};
    case ACTION_TYPES.REGISTER_REQUEST:
      return {
        loggedIn: true,
        username: action.username
      };
    case ACTION_TYPES.REGISTER_SUCCESS:
      return {
        loggedIn: true,
        username: action.username
      };
    case ACTION_TYPES.REGISTER_FAILURE:
      return {}

    default:
      return state
  }

}
