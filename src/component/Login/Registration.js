import React, { Component } from 'react'
import '../../css/page/registration.css'
import { connect } from 'react-redux'

import { userAction } from '../../redux/actions/userActions'

class Registration extends Component {
  constructor (props) {
    super(props)
    this.state = {
      firstName:'',
      lastName:'',
      username:'',
      password:'',
      submitted:false

    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)



  }
  handleChange(e){
    const {name,value} = e.target
    this.setState({[name]: value})
  }
  handleSubmit(event){
    event.preventDefault()
    this.setState({submitted:true})
    const {firstName,lastName,username,password} = this.state
    const {dispatch }= this.props
    if (firstName && lastName && username && password){
      dispatch(userAction.signup(this.props,firstName,lastName,username,password))
    }
  }


  render () {
    const {loggedIn} = true
    const {firstName,lastName,username,password,submitted} = this.state
    return (
      <section className={'account-page'}>
        <header className={'account-page'}>
          <div className={'account-page-title'}>
            <h1>Compte utilisateur</h1>
          </div>
        </header>
        <main>
          <div className={'user-container'}>
            <form className={'user-info'} onSubmit={this.handleSubmit}>
              <div className={'edit-account'}>
                <div className={'form-item-email'+ (submitted && !firstName ? 'hos-error' : '')}>
                  <label htmlFor="firstName">NOM *</label>
                  <input type="text" className={'edit-email'} onChange={this.handleChange}/>
                  {submitted && !firstName &&
                  <div className={'required'}>Nom is required</div>
                  }

                </div>
                <div className={'form-item-email' + (submitted && !lastName ? 'hos-error' : '')}>
                  <label htmlFor="lastName">PRENOM *</label>
                  <input type="text" className={'edit-email'} onChange={this.handleChange}/>
                  {
                    submitted && !lastName &&
                    <div className={'required'}>Prénom is required</div>
                  }

                </div>
                <div className={'form-item-email' + (submitted && !username ? 'has-error' : '')}>
                  <label htmlFor="edit-email">ADRESSE E-MAIL *</label>
                  <input type="email" className={'edit-email'} onChange={this.handleChange}/>
                  {submitted && !username &&
                  <div className={'required'}>Email is required</div>
                  }

                </div>
                <div className={'form-item-pass'}></div>
                <div className={'form-item-pass1'}>
                  <div className={'form-item-pass-strength'}>

                    <div className={'form-item-pass-strength-title'}>Sécurité du
                      mot de passe :
                    </div>
                    <div className={'password-indicator'}>
                      <div className={'indicator'}></div>
                    </div>
                  </div>
                  <label htmlFor="password">MOT DE PASSE *</label>
                  <input type="password" onChange={this.handleChange}/>
                  {submitted && !password &&
                  <div className={'required'}>Mot de passe is required</div>
                  }

                </div>
                <div className={'form-item-pass-confirm' + (submitted && !password ? 'hos-error' : '')}>
                  <label htmlFor="password">Confirmer Le MOT DE PASSE *</label>
                  <input type="password" name={'password'} onChange={this.handleChange}/>
                  {submitted && !password &&
                  <div className={'required'}>Mot de passe is required</div>
                  }
                </div>

              </div>
              <button type={'submit'} className={'editt-submit'}>CREE UN NOUVEAU
                COMPTE
              </button>

            </form>

          </div>
        </main>
      </section>
    )
  }
}

export default Registration
