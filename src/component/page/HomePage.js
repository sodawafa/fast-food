import React, { Component } from 'react'
import '../../css/page/homepage.css'
import Logo from '../header/Logo'
import { Link } from 'react-router-dom'
import sandwich from '../../assets/sandwichIcon.png'
import plat from '../../assets/platIcon.png'
import dessert from '../../assets/dessertIcon.png'
import sign from '../../assets/userSeign.png'
class HomePage extends Component {
  constructor (props) {
    super(props)
  }
  render () {
    return(
      <div>
      <nav className={'main_nav'}>
        <Link to={"/home"} className={'main_nav_logo'}><Logo/></Link>
        <div>

          <Link className="main_nav_item" to="/login">
            <img src={sign}  className={'icon'} alt="sign"/>
            Sign In
          </Link>
        </div>

      </nav>
      <main>
        <div className={'hero'}>

        </div>
        <section className={'features'}>
          <div className={'feature-item'}>
            <img src={sandwich} alt="chat"/>
            <h3  className={'feature-item-title'}>Des Sandwiches trop délicieux</h3>
            <p>Le sandwich est un mets, généralement composé de deux ou plusieurs tranches de pain,<br/> avec un ou plusieurs ingrédients entre elles.</p>

          </div>
          <div className={'feature-item'}>
            <img src={plat} alt="chat"/>
            <h3  className={'feature-item-title'}>Des Plats Gourmands</h3>
            <p>Nos plâts sont harmonieux,délicieux et trop Gourmands ils contients des aliments frais et bio </p>

          </div>
          <div className={'feature-item'}>
            <img src={dessert} alt="chat"/>
            <h3  className={'feature-item-title'}> Des Desserts léger</h3>
            <p> Nos desserts ont Humer une douce odeur de printemps et un gout moelleux, mou et léger et une Dégustation merveille et qui fond en bouche</p>

          </div>

        </section>



   </main>
      </div>

    )
  }
}
export default HomePage
