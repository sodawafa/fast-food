import React ,{ Component} from 'react'
import api from '../api'
import '../css/container/energie.css'
import  user from '../assets/user.png'
import { Link } from 'react-router-dom'
class Energie extends Component{
  constructor (props) {
    super(props)
    this.state = {
      energie: [],
    }
  }
  componentDidMount () {
    api('/energie/' + this.props.id).then(json => {
      this.setState({ energie: json.data })
      console.log(this.state.energie)
    })
  }

  render () {
    return(
      <div>
        <header className={'header-content'}>
          <div className={'header-content-title'}>
            <h1>La Nutrition</h1>
            <p>BON Â MANGER,BON Â SAVOIR</p>
          </div>
          <Link to="/loginprofile" className={'account'}>
            <span><img src={user} alt=""/></span>
            <div>Connexion/Inscription</div>
          </Link>

        </header>

      <div className={'arrow-content'}>

        <table>
     <thead>
     <tr>
       <th colSpan={"8"}>Valeur Energetique Pour 100g</th>
     </tr>
     </thead>
          <tbody>
          <tr>
           <td>{this.state.energie.name}</td>
           {this.state.energie.energies && this.state.energie.energies.map((elem,index) => {
              return <td key={'energie_content_' + index}>{elem}</td>
            })}
          </tr>
          </tbody>
        </table>
        <div className={'pic'}>
          {
            this.state.energie.picture&&
            <img src={require(`./../assets${this.state.energie.picture}`).default} alt=""/>
          }
            <div className={'text'}>{this.state.energie.description}</div>



        </div>

      </div>
      </div>
    )
  }
}
export default Energie
