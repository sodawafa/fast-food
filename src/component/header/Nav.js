import '../../css/header/nav.css'
import React from 'react'
import { Link, useLocation } from 'react-router-dom'

function getClass (currentPathname, pathname) {
  if (currentPathname === pathname) return 'active'
  return ''
}

export default function Nav() {
  const location = useLocation()
  return(
    <div className={"header"}>
    <nav className={'header-nav'}>
      <ul>
       {/* <li className={'header-nav-item'}>
          <Link to="/accueil" className={getClass(location.pathname, "/accueil")}>ACCUEIL</Link>
        </li>*/}
      {/*  <li className={'header-nav-item'}>
          <Link to="/products" className={getClass(location.pathname, "/products")}>PRODUITS</Link>
        </li>*/}
        <li className={'header-nav-item'}>
          <Link to='receipts' className={getClass(location.pathname, "/receipts")}>RECETTES</Link>
        </li>
        <li className={'header-nav-item'}>
          <Link to='/energies' className={getClass(location.pathname, "/energies")}>Nutrition</Link>
        </li>
        <li className={'header-nav-item'}>
          <Link to='/entreprise' className={getClass(location.pathname, "/entreprise")}>L'ENTREPRISE</Link>
        </li>
        <li className={'header-nav-item'}>
          <Link to='/recrutement' className={getClass(location.pathname, "/recrutement")}>RECRUTEMENT</Link>
        </li>
      </ul>

    </nav>
    </div>
  )
}

