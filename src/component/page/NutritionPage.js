import React, { Component } from 'react'
import '../../css/page/nutritionPage.css'
import api from '../../api'
import { Link } from 'react-router-dom'

class NutritionPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      energies: [],
    }
  }

  componentDidMount () {
    api('/energies').then(json => {
      this.setState({
        energies: json.data,
      })
    })
  }

  render () {
    return (
      <div>
        <header className={'header-content'}>
          <div className={'header-content-title'}>
            <h1>La Nutrition</h1>
            <p>BON Â MANGER,BON Â SAVOIR</p>
          </div>

        </header>
        <main>
          <div className={'content-energies'}>
            {
              this.state.energies &&
              this.state.energies.map((energie, index) => {
                return (
                  <ul key={'energie_' + energie.id + index.id}>
                    <Link to={`/energie/${energie.id}`}>
                      <li>{energie.name}</li>
                    </Link>

                  </ul>
                )

              })
              }
              </div>

              </main>

              </div>
              )
            }
            }

  export default NutritionPage
